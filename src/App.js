import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { characters: [] };
  }

  componentDidMount() {
    fetch(
      '/api/people/',
      {
        method: "GET",
        headers: {
          'Access-Control-Allow-Origin':'*',
          'Content-Type': 'application/json',
        }
      })
      .then(response =>  response.json())
      .then(result =>  { this.setState({ characters: result.results })})
      .catch(err => { console.log(err)})
  }

  render() {
    const { characters } = this.state;
    console.log(characters);
    return (
      <div className="App">
      </div>
    );
  }
}

export default App;
